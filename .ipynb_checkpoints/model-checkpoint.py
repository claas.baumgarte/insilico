import numpy as np
from scipy.integrate import odeint
from pyqtgraph.Qt import QtCore
import random

class kineticSolver():

    def __init__(self):
        self.Tgui = 20 #[°C]
        self.Treal = 20 #[°C]
        self.kinetic = 0
        self.Ca_0 = 0.1 #[mol/l]
        self.Cb_0 = 0.1 #[mol/l]
        self.flow_aGui = 0 #[ml/min]
        self.flow_bGui = 0 #[ml/min]
        self.flow_aReal = 0 #[ml/min]
        self.flow_bReal = 0 #[ml/min]
        self.cp = 0 #[mol/l]
        self.ca = 0 #[mol/l]
        self.cb = 0 #[mol/l]
        self.dwell = 0 #[min]
        self.reactorvolume = 100 #[ml]
        self.calculateLoop()

    def setTgui(self, Tgui):
        self.Tgui = Tgui

    def getTreal(self):
        return self.Treal

    def setKinetic(self, kinetic):
        self.kinetic = kinetic

    def setCa_0(self, new):
        self.Ca_0 = new

    def setCb_0(self, new):
        self.Cb_0 = new

    def setFlow_aGui(self, new):
        self.flow_aGui = new

    def setFlow_bGui(self, new):
        self.flow_bGui = new

    def getFlow_aReal(self):
        return self.flow_aReal

    def getFlow_bReal(self):
        return self.flow_bReal

    def getCp(self):
        return self.cp

    def getCa(self):
        return self.ca

    def getCb(self):
        return self.cb

    def calculateLoop(self):
        self.timer = QtCore.QTimer()
        self.timer.timeout.connect(self.calculate)
        self.timer.start(0)

    def calculate(self):
        # real Values (rework later)
        self.calcTreal(self.Tgui)
        self.calcFlowAreal(self.flow_aGui)
        self.calcFlowbreal(self.flow_bGui)
        self.dwell = self.calc_dwell(self.flow_aReal, self.flow_bReal,
                                     self.reactorvolume)
        self.iniConditions = self.calc_iniConditions(self.flow_aReal,
                                                     self.flow_bReal,
                                                     self.Ca_0,
                                                     self.Cb_0, self.Treal)
        exitConcentration = self.calc_exitConcentration(self.kinetic,
                                                        self.iniConditions,
                                                        self.dwell)
        cp = exitConcentration[2]
        ca = exitConcentration[0]
        cb = exitConcentration[1]
        # add random number
        rnd = random.randrange(-20, 20, 1)/1000
        cp = cp * (1 + rnd)
        rnd = random.randrange(-20, 20, 1)/1000
        ca = ca * (1 + rnd)
        rnd = random.randrange(-20, 20, 1)/1000
        cb = cb * (1 + rnd)

        self.cp = cp
        self.ca = ca
        self.cb = cb

    def calc_exitConcentration(self, kinetic, iniConditions, dwell):
        t = np.linspace(0, self.dwell, 1000)
        if kinetic == 0:
            y = odeint(self.scnd, iniConditions, t)
        if kinetic == 1:
            y = odeint(self.noreact, iniConditions, t)
        if kinetic == 2:
            y = odeint(self.pow, iniConditions, t)
        exitConcentration = [y[200, 0], y[200, 1],
                             y[200, 2]]
        return exitConcentration

# calculate process parameters from initial Values
    def calc_iniConditions(self, flow_a, flow_b, c_a0, c_b0, T):
        flow = flow_a+flow_b
        """
        dity fix for ZeroDivisionError
        """
        if flow == 0:
            flow += 0.00000000001
        c_a1 = c_a0*flow_a/(flow)
        c_b1 = c_b0*flow_b/(flow)
        iniConditions1 = [c_a1, c_b1, 0, T]
        return iniConditions1

    def calc_dwell(self, flow_a, flow_b, reactorvolume):
        flow = flow_a+flow_b
        """
                dirty fix for ZeroDivisionError
        """
        try:
            dwell = reactorvolume/flow
        except:
            dwell = 1000000000000000000000
            # print("flow = 0  ==> dwell is infineit big")
        return dwell

        """
Smothen aout the curves here?
        """
    def calcTreal(self, Tgui):
        Treal = Tgui
        self.Treal = Treal

    def calcFlowAreal(self, flowAgui):
        flowA = flowAgui
        self.flow_aReal = flowA

    def calcFlowbreal(self, flowBgui):
        flowB = flowBgui
        self.flow_bReal = flowB


# kinetics
    def scnd(self, z, t):
        # A + B -> P
        a = z[0]
        b = z[1]
        p = z[2]
        T = z[3]
        k = 40*np.exp(-20/T)
        dadt = -k*a*b
        dbdt = -k*a*b
        dpdt = k*a*b
        dTdt = 0
        return [dadt, dbdt, dpdt, dTdt]

    def noreact(self, z, t):
        return [0, 0, 0, 0]
    
    def pow(self, z, t):
        # 2A + B -> P
        a=z[0]
        b=z[1]
        p=z[2]
        T = z[3]
        k=40*np.exp(-23/T)
        dadt = -2*k*a*a*b
        dbdt =-k*a*a*b
        dpdt = k*a*a*b
        dTdt = 0
        return [dadt,dbdt,dpdt, dTdt]


'''
Idea:
function to smothen out the output value Graph:
Write the last X exit exitConcentration values to an Array and calculate
the average value of the array. Implement the dewll time as a delay time,
remove .round(3) operator in calc_exitConcentration and round after smothening.
Maybe add some measuring inaccuracies.
'''
