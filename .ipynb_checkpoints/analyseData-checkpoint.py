import numpy as np
import matplotlib.pyplot as plt
import csv
import pandas as pd

# Parameter:
reactorvolume = 100

df = pd.read_csv("testdata2.csv")
# Indexverschiebung für Kinetik beseitigen
df['kinetic'] = df['kinetic'] + 1

# Startkonzentrationen berechnen
df['flow'] = df['flow_a'] + df['flow_b']
df['cA1'] = (df['cA_0'] * df['flow_a']) / df['flow']
df['cB1'] = (df['cB_0'] * df['flow_b']) / df['flow']

# Verweilzeit berechnen
df['dwell'] = reactorvolume / df['flow']
df = df.sort_values('dwell', ascending=False)

# sortieren nach Startkonzentration
print("Für welche Startkonzentration cA1 sollen die Daten geplottet werden?")
df01 = df[df['cA_0'] == 0.1]
df05 = df[df['cA_0'] == 0.5]
df1 = df[df['cA_0'] == 1]

#%matplotlib inline
df01.plot(x='dwell', y=['cP', 'cA2', 'cB2'])
plt.xlabel('dwell')
plt.ylabel('c')
plt.xlabel('dwell')
plt.title("cA0 = cB0 = 0.1")
plt.show()

df05.plot(x='dwell', y=['cP','cA2','cB2'])
plt.xlabel('dwell')
plt.ylabel('c')
plt.xlabel('dwell')
plt.title("cA0 = cB0 = 0.5")
plt.show()

df1.plot(x='dwell', y=['cP', 'cA2', 'cB2'])
plt.xlabel('dwell')
plt.ylabel('c')
plt.xlabel('dwell')
plt.title("cA0 = cB0 = 1")
plt.show()
