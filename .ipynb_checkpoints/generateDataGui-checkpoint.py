"""
Erstellt automatisiert mit der inSicico GUI
Daten um diese auf Konsistenz prüfen zu können.
"""
import time
import pyautogui

""""
time.sleep(5)
print(pyautogui.position())
"""


def setCa_0(ca):
    if ca == 0.1:
        pyautogui.click(x=204, y=68)
    if ca == 0.5:
        pyautogui.click(x=202, y=87)
    if ca == 1:
        pyautogui.click(x=201, y=104)
    else:
        print("Ungültige Eingabe für cA_0")


def setCb_0(ca):
    if ca == 0.1:
        pyautogui.click(x=412, y=66)
    if ca == 0.5:
        pyautogui.click(x=411, y=87)
    if ca == 1:
        pyautogui.click(x=411, y=104)
    else:
        print("Ungültige Eingabe für cB_0")


def setFlowA(newFlow):
    time.sleep(0.5)
    pyautogui.click(x=212, y=489)
    pyautogui.typewrite(str(newFlow))
    time.sleep(0.1)


def setFlowB(newFlow):
    time.sleep(0.5)
    pyautogui.click(x=420, y=490)
    pyautogui.typewrite(str(newFlow))
    time.sleep(0.1)


def setT(newT):
    time.sleep(0.5)
    pyautogui.click(x=72, y=492)
    pyautogui.typewrite(str(newT))
    time.sleep(0.5)


def saveData():
    time.sleep(0.3)
    pyautogui.mouseDown(x=710, y=581)
    time.sleep(0.1)
    pyautogui.mouseUp(x=710, y=581)
    time.sleep(0.1)


def exportData():
    time.sleep(0.2)
    pyautogui.click(x=869, y=580, clicks=1)


def setKinetic(kinetic):
    time.sleep(0.5)
    pyautogui.click(x=106, y=129)
    if kinetic == 1:
        pyautogui.click(x=106, y=129)
    if kinetic == 2:
        pyautogui.click(x=108, y=143)
    else:
        print("Falsche Kinetik angegeben")

        

time.sleep(10)
for T in [20, 50, 100]:
    setT(T)
    for c0 in [0.1, 0.5, 1]:
        setCa_0(c0)
        setCb_0(c0)
        for flow in range(0, 100, 1):
            setFlowA(flow)
            setFlowB(flow)
            saveData()
exportData()
