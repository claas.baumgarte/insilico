from random import randrange
from pyqtgraph.Qt import QtCore



class updater():
    def __init__(self, guiWindow, kineticSolver):
        self.timer = QtCore.QTimer()
        self.timer.timeout.connect(lambda: self.update(guiWindow,
                                   kineticSolver))
        self.timer.start(0)

    def update(self, gui, kineticSolver):
        kineticSolver.setTgui(gui.getT())
        kineticSolver.setKinetic(gui.getKinetic())
        kineticSolver.setCa_0(gui.getCa_0())
        kineticSolver.setCb_0(gui.getCb_0())
        kineticSolver.setFlow_aGui(gui.getFlow_a())
        kineticSolver.setFlow_bGui(gui.getFlow_b())

        gui.setTreal(kineticSolver.getTreal())
        gui.setFlow_aReal(kineticSolver.getFlow_aReal())
        gui.setFlow_bReal(kineticSolver.getFlow_bReal())
        gui.setCp(kineticSolver.getCp())
        gui.setCa(kineticSolver.getCa())
        gui.setCb(kineticSolver.getCb())
