import csv
from model import kineticSolver

def calcData(ks, kinetic, flow_a, flow_b, T, cA_0, cB_0):
    # enter Data to kinetic Solver
    ks.setKinetic(kinetic)
    ks.setFlow_aGui(flow_a)
    ks.setFlow_bGui(flow_b)
    ks.setTgui(T)
    ks.setCa_0(cA_0)
    ks.setCb_0(cB_0)
    # calculate exit Data
    ks.calculate()
    # write exit Data to variables
    cP = ks.getCp()
    cA2 = ks.getCa()
    cB2 = ks.getCb()
    return [kinetic, flow_a, flow_b, T, cA_0, cB_0, cP, cA2, cB2]

def createEmptyDataDocument():
    header = ['kinetic', 'flow_a [ml/min]', 'flow_b [ml/min]', 'T [°C]', 'cA_0 [mol/l]', 'cB_0 [mol/l]', 'cP [mol/l]','cA2 [mol/l]', 'cB2 [mol/l]']
    with open('data.csv', 'w') as csv_file:
        csv_writer = csv.writer(csv_file, delimiter=',')
        csv_writer.writerow(header)

def saveData(data):
    #kinetic index +1
    data[0] = data[0] + 1
    with open('data.csv', 'a') as csv_file:
        csv_writer = csv.writer(csv_file, delimiter=',')
        csv_writer.writerow(data)


def generateData():
    ks = kineticSolver()
    createEmptyDataDocument()
    for kinetic in [0, 1, 2]:
        for T in [20, 50, 100]:
            for c0 in [0.1, 0.5, 1]:
                cA_0 = c0
                cB_0 = c0
                for flow in range(0, 100):
                    flow_a = flow
                    flow_b = flow
                    data = calcData(ks, kinetic, flow_a, flow_b, T, cA_0, cB_0)
                    saveData(data)
                    
generateData()