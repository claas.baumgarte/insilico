
import sys
import os
from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import (QApplication, QCheckBox, QGridLayout, QGroupBox,
                             QMenu, QPushButton, QRadioButton, QVBoxLayout,
                             QWidget, QSlider, QLabel, QComboBox, QSpinBox,
                             QTableWidget, QHBoxLayout, QFileDialog,
                             QTableWidgetItem, QTableView)
import pyqtgraph as pg
from pyqtgraph.Qt import QtGui, QtCore
import numpy as np
import csv
from shutil import copyfile
import unicodecsv

"""
in communication.py wird die Ausgangskonzentration unm den Faktor 10.000
verstärkt um im Graphen sichtbar zu sein!!
unterschiedliche Skalen für die Graphen implementieren und dann den Faktor
 entfernen!!!
"""


class inSilico_GUI(QWidget):

    def __init__(self):
        super().__init__()
        self.setWindowTitle('inSilico')
        self.maingrid()
        self.show()
        self.plotter()
        # set default Input Values
        self.kinetic = 0
        self.ca_0 = 0.1
        self.cb_0 = 0.1
        self.flow_a = 0
        self.flow_b = 0
        self.T = 20
        self.maxFlow = 100
        self.cp = 0
        self.ca = 0
        self.cb = 0
        self.flow_aReal = 0
        self.flow_bReal = 0
        self.Treal = 20
        self.createEmptyDataDocument()
        # "reale" werte für Regelgrößen hinzufügen
        # (hier und auch bei den Plots)
        # reactorvolume

    def getKinetic(self):
        return self.kinetic

    def setKinetic(self, newKinetic):
        self.kinetic = newKinetic
        print("new Kinetic selected: Kinetic " + str(newKinetic + 1))
        # "Kinetic 1" in the GUI <=> self.kinetic = 0

    def getCa_0(self):
        return self.ca_0

    def setCa_0(self, newCa_0):
        self.ca_0 = newCa_0
        print("new Concentration cA selected:  " + str(newCa_0))

    def getCb_0(self):
        return self.cb_0

    def setCb_0(self, newCb_0):
        self.cb_0 = newCb_0
        print("new Concentration cB selected:  " + str(newCb_0))

    def getFlow_a(self):
        return self.flow_a

    def setFlow_a(self, newFlowA):
        self.flow_a = newFlowA

    def getFlow_b(self):
        return self.flow_b

    def setFlow_b(self, newFlowB):
        self.flow_b = newFlowB

    def getT(self):
        return self.T

    def setT(self, newT):
        self.T = newT
        print("Temperatur: " + str(newT))

    def setFlowSliderA(self, sliderValue):
        self.setFlow_a(sliderValue)
        print("Flow of Component A changed to "+str(sliderValue))

    def setFlowSliderB(self, sliderValue):
        self.setFlow_b(sliderValue)
        print("Flow of Component B changed to "+str(sliderValue))

    def setCp(self, newCP):
        self.cp = newCP

    def setCa(self, newCa):
        self.ca = newCa

    def setCb(self, newCb):
        self.cb = newCb

    def getCp(self):
        return self.cp

    def setTreal(self, new):
        self.Treal = new

    def setFlow_aReal(self, new):
        self.flow_aReal = new

    def setFlow_bReal(self, new):
        self.flow_bReal = new

    def maingrid(self):
        maingrid = QGridLayout()
        maingrid.addWidget(self.createInfoGroup(), 1, 0)
        maingrid.addWidget(self.createInputGroup(), 0, 0)
        maingrid.addWidget(self.createPlotGroup(), 0, 1)
        maingrid.addWidget(self.createDataGroup(), 1, 1)
        self.setLayout(maingrid)
        self.resize(1300, 800)

    def createPlotGroup(self):
        plotGroup = QGroupBox("Live Data")
        vbox = QHBoxLayout()
        self.plotWidget = pg.PlotWidget()
        vbox.addWidget(self.plotWidget)
        plotGroup.setLayout(vbox)

        # test for multiple Axis
        # Axis
        a2 = pg.AxisItem("left")
        a3 = pg.AxisItem("left")
        a4 = pg.AxisItem("left")
        a5 = pg.AxisItem("left")
        a6 = pg.AxisItem("left")

        # ViewBoxes
        v2 = pg.ViewBox()
        v3 = pg.ViewBox()
        v4 = pg.ViewBox()
        v5 = pg.ViewBox()
        v6 = pg.ViewBox()

        # main view
        pw = pg.GraphicsView()
        # pw.setWindowTitle('pyqtgraph example: multiple y-axis')
        pw.show()
        return plotGroup

    def plotter(self):
        """
        self.dataT = [0]
        self.curveT = self.plotWidget.getPlotItem().plot(pen=(255, 0, 0))

        self.dataFlowA = [0]
        self.curveFlowA = self.plotWidget.getPlotItem().plot(pen=(0, 0, 255))

        self.dataFlowB = [0]
        self.curveFlowB = self.plotWidget.getPlotItem().plot(pen=(0, 255, 0))
        """
        self.dataCp = [0]
        self.curveCp = self.plotWidget.getPlotItem().plot()

        self.timer = QtCore.QTimer()
        self.timer.timeout.connect(self.updater)
        self.timer.start(0)

    def updater(self):
        """
        self.dataT.append(self.getT())
        self.curveT.setData(self.dataT)

        self.dataFlowA.append(self.getFlow_a())
        self.curveFlowA.setData(self.dataFlowA)

        self.dataFlowB.append(self.getFlow_b())
        self.curveFlowB.setData(self.dataFlowB)
        """
        self.dataCp.append(self.getCp())
        self.curveCp.setData(self.dataCp)

    def createInputGroup(self):
        inputGroup = QGroupBox("Input Values")

        kinetic_Selection = QGroupBox("Kinetic:")
        vbox = QVBoxLayout()
        kinetic_ComboBox = QComboBox()
        kinetic_ComboBox.addItems(["Kinetic 1", "Kinetic 2", "Kinetic 3"])
        vbox.addWidget(kinetic_ComboBox)
        kinetic_Selection.setLayout(vbox)
        kinetic_ComboBox.currentIndexChanged.connect(self.setKinetic)

        c_a_Selection = QGroupBox("cA_0:")
        vbox = QVBoxLayout()
        radio_ca0_1 = QRadioButton("0,1 mol/l")
        radio_ca0_1.setChecked(True)
        radio_ca0_2 = QRadioButton("0,5 mol/l")
        radio_ca0_3 = QRadioButton("1   mol/l")
        vbox.addWidget(radio_ca0_1)
        vbox.addWidget(radio_ca0_2)
        vbox.addWidget(radio_ca0_3)
        c_a_Selection.setLayout(vbox)
        radio_ca0_1.toggled.connect(lambda: self.setCa_0(0.1))
        radio_ca0_2.toggled.connect(lambda: self.setCa_0(0.5))
        radio_ca0_3.toggled.connect(lambda: self.setCa_0(1))

        c_b_Selection = QGroupBox("cB_0:")
        vbox = QVBoxLayout()
        radio_cb0_1 = QRadioButton("0,1 mol/l")
        radio_cb0_1.setChecked(True)
        radio_cb0_2 = QRadioButton("0,5 mol/l")
        radio_cb0_3 = QRadioButton("1 mol/l")
        vbox.addWidget(radio_cb0_1)
        vbox.addWidget(radio_cb0_2)
        vbox.addWidget(radio_cb0_3)
        c_b_Selection.setLayout(vbox)
        radio_cb0_1.toggled.connect(lambda: self.setCb_0(0.1))
        radio_cb0_2.toggled.connect(lambda: self.setCb_0(0.5))
        radio_cb0_3.toggled.connect(lambda: self.setCb_0(1))

        T_Selection = QGroupBox("Temperatur [°C]:")
        vbox = QVBoxLayout()
        slider_T = QSlider(Qt.Vertical)
        slider_T.setValue(20)
        slider_T.setMinimum(20)
        slider_T.setMaximum(300)
        slider_T.setTickInterval(1)
        slider_T.setSingleStep(1)
        spinBox_T = QSpinBox()
        # implement temperatur Range as an attribute of the class later
        spinBox_T.setValue(20)
        spinBox_T.setMinimum(20)
        spinBox_T.setMaximum(300)
        vbox.addWidget(slider_T)
        vbox.addWidget(spinBox_T)
        T_Selection.setLayout(vbox)
        slider_T.valueChanged.connect(self.setT)
        slider_T.valueChanged.connect(spinBox_T.setValue)
        spinBox_T.valueChanged.connect(slider_T.setValue)

        flow_a_selection = QGroupBox("Flow Component A [ml/min]:")
        vbox = QVBoxLayout()
        slider_FlowA = QSlider(Qt.Vertical)
        slider_FlowA.setValue(0)
        # imlement the maximum as an atribut of class later
        slider_FlowA.setMaximum(100)
        slider_FlowA.setTickInterval(1)
        slider_FlowA.setSingleStep(1)
        spinBox_FlowA = QSpinBox()
        # maybe grey out the spinnbox (no typing)
        spinBox_FlowA.setValue(0)
        spinBox_FlowA.setMinimum(0)
        spinBox_FlowA.setMaximum(100)
        vbox.addWidget(slider_FlowA)
        vbox.addWidget(spinBox_FlowA)
        flow_a_selection.setLayout(vbox)
        slider_FlowA.valueChanged.connect(self.setFlowSliderA)
        slider_FlowA.valueChanged.connect(spinBox_FlowA.setValue)
        spinBox_FlowA.valueChanged.connect(slider_FlowA.setValue)

        flow_b_selection = QGroupBox("Flow Component B [ml/min]:")
        vbox = QVBoxLayout()
        slider_FlowB = QSlider(Qt.Vertical)
        slider_FlowB.setValue(0)
        # imlement the maximum as an atribut of the Object later
        slider_FlowB.setMaximum(100)
        slider_FlowB.setTickInterval(1)
        slider_FlowB.setSingleStep(1)
        spinBox_FlowB = QSpinBox()
        # maybe grey out the spinnbox (no typing)
        spinBox_FlowB.setValue(0)
        spinBox_FlowB.setMinimum(0)
        spinBox_FlowB.setMaximum(100)
        vbox.addWidget(slider_FlowB)
        vbox.addWidget(spinBox_FlowB)
        flow_b_selection.setLayout(vbox)
        slider_FlowB.valueChanged.connect(self.setFlowSliderB)
        slider_FlowB.valueChanged.connect(spinBox_FlowB.setValue)
        spinBox_FlowB.valueChanged.connect(slider_FlowB.setValue)

        inputLayout = QGridLayout()
        inputLayout.addWidget(kinetic_Selection, 0, 0)
        inputLayout.addWidget(c_a_Selection, 0, 1)
        inputLayout.addWidget(c_b_Selection, 0, 2)
        inputLayout.addWidget(T_Selection, 1, 0)
        inputLayout.addWidget(flow_a_selection, 1, 1)
        inputLayout.addWidget(flow_b_selection, 1, 2)
        inputGroup.setLayout(inputLayout)
        return inputGroup

    def createDataGroup(self):
        dataBox = QGroupBox("Data")
        # model for data import
        self.model = QtGui.QStandardItemModel(self)
        self.dataTable = QTableView(self)
        self.datasets = 0
        self.dataTable.setModel(self.model)
        tableItem = QTableWidgetItem()
        # self.dataTable.columnLabels = ["Kinetik", "Flow A[ml/min]", "Flow B[ml/min]", "T[°C]",
        #                                "cA_0[mol/l]", "cB_0[mol/l]", "cPcB_0[mol/l]"]
        # self.dataTable.setHorizontalHeaderLabels(self.dataTable.columnLabels)
        buttonSave = QPushButton("Save current Values")
        buttonSave.clicked.connect(lambda: self.saveData())
        buttonExport = QPushButton("Export Data")
        buttonExport.clicked.connect(lambda: self.exportData())

        topLayout = QHBoxLayout()
        topLayout.addWidget(buttonSave)
        topLayout.addWidget(buttonExport)
        topLayout.addStretch(1)

        dataLayout = QGridLayout()
        dataLayout.addLayout(topLayout, 0, 0, 1, 2)
        dataLayout.addWidget(self.dataTable, 1, 0)
        dataBox.setLayout(dataLayout)
        return dataBox

    def createEmptyDataDocument(self):
        header = ['kinetic', 'flow_a [ml/min]', 'flow_b [ml/min]', 'T [°C]', 'cA_0 [mol/l]', 'cB_0 [mol/l]', 'cP [mol/l]','cA2 [mol/l]', 'cB2 [mol/l]']
        with open('data.csv', 'w') as csv_file:
            csv_writer = csv.writer(csv_file, delimiter=',')
            csv_writer.writerow(header)

    def saveData(self):
        data = [self.getKinetic(), self.flow_aReal, self.flow_bReal,
                self.Treal, self.getCa_0(), self.getCb_0(), self.cp,
                self.ca, self.cb]
        #kinetic index +1
        data[0] = data[0] + 1
        with open('data.csv', 'a') as csv_file:
            csv_writer = csv.writer(csv_file, delimiter=',')
            csv_writer.writerow(data)
        self.showData()

    def exportData(self):
        options = QFileDialog.Options()
        # options |= QFileDialog.DontUseNativeDialog
        fileName, _ = QFileDialog.getSaveFileName(self,"QFileDialog.getSaveFileName()","","All Files (*);;CSV Files (*.CSV)", options=options)
        if fileName:
            print("Data exportet to " + fileName + ".csv")
            destination = os.getcwd() + '/data.csv'
            copyfile(destination, fileName + ".csv")
            """
            Achtung! Gleichnamige Dateien werden ungefragt überschrieben!
            """

    def showData(self):
        self.model.clear()
        with open('data.csv', "r") as fileInput:
            for row in csv.reader(fileInput):
                items = [
                        QtGui.QStandardItem(field)
                        for field in row
                        ]
                self.model.appendRow(items)

    def createInfoGroup(self):
        infoBox = QGroupBox("Information")
        vbox = QHBoxLayout()
        text = QLabel()
        infoText = "Welcome to the inSilico reaction Simulation tool."
        text.setText(infoText)
        vbox.addWidget(text)
        infoBox.setLayout(vbox)
        return infoBox




# run GUI
if __name__ == '__main__':

    app = QApplication(sys.argv)
    GUI = inSilico_GUI()
    sys.exit(app.exec_())
