from PyQt5.QtWidgets import QWidget, QApplication, QLabel, QHBoxLayout
import sys


class errorwindow(QWidget):

    def __init__(self, errormessage):
        super().__init__()
        self.setWindowTitle('inSilico - Error')
        vbox = QHBoxLayout()
        label = QLabel(errormessage)
        vbox.addWidget(label)
        self.setLayout(vbox)
        self.show()


"""
if __name__ == '__main__':

    app = QApplication(sys.argv)
    GUI = errorwindow("Error 42")
    sys.exit(app.exec_())
"""
