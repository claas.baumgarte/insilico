
import sys
import os
from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import (QApplication, QCheckBox, QGridLayout, QGroupBox,
                             QMenu, QPushButton, QRadioButton, QVBoxLayout,
                             QWidget, QSlider, QLabel, QComboBox, QSpinBox,
                             QTableWidget, QHBoxLayout, QFileDialog,
                             QTableWidgetItem, QTableView, QTabWidget)
import pyqtgraph as pg
from pyqtgraph.Qt import QtGui, QtCore
import numpy as np
import csv
from shutil import copyfile
import unicodecsv
import time
from threading import Thread

"""
in communication.py wird die Ausgangskonzentration unm den Faktor 10.000
verstärkt um im Graphen sichtbar zu sein!!
unterschiedliche Skalen für die Graphen implementieren und dann den Faktor
 entfernen!!!
"""


class inSilico_GUI(QWidget):

    def __init__(self):
        super().__init__()
        self.setWindowTitle('inSilico')
        self.maingrid()
        self.show()
        # self.plotter()
        # set default Input Values
        self.kinetic = 0
        self.ca_0 = 0
        self.T = 250
        self.maxFlow = 100
        self.cp = 0
        self.ca = 0
        self.cb = 0
        self.Treal = 20
        self.createEmptyDataDocument()
        self.dataCp1 = []
        self.dataTime1 = []
        self.dataCp2 = []
        self.dataTime2 = []
        self.dataCpSaved1 = []
        self.dataTimeSaved1 = []
        self.dataCpSaved2 = []
        self.dataTimeSaved2 = []
        self.dataIndex = 0
        self.start = 0 
        self.data = [[],[]]
        self.rausch = []
        self.detTr = []
        self.plotIntervall= 1 #delta t zwischen den Messungen
        self.timecompression = 1
        # "reale" werte für Regelgrößen hinzufügen
        # (hier und auch bei den Plots)
        # reactorvolume

    def getKinetic(self):
        return self.kinetic
    
    def setData(self, data):
        self.data = data

    def setKinetic(self, newKinetic):
        self.kinetic = newKinetic
        print("new Kinetic selected: Kinetic " + str(newKinetic + 1))
        # "Kinetic 1" in the GUI <=> self.kinetic = 0

    def getCa_0(self):
        return self.ca_0

    def setCa_0(self, newCa_0):
        self.ca_0 = newCa_0
        print("new Concentration cA selected:  " + str(newCa_0))
        
    def setCaSlider(self, sliderValue):
        self.setCa_0(sliderValue)
        print("Ca0 changed to "+str(sliderValue))

    def getT(self):
        return self.T

    def setT(self, newT):
        self.T = newT
        print("Temperatur: " + str(newT))

    def setCp(self, newCP):
        self.cp = newCP

    def setCa(self, newCa):
        self.ca = newCa

    def setCb(self, newCb):
        self.cb = newCb

    def getCp(self):
        return self.cp

    def setTreal(self, new):
        self.Treal = new


    def maingrid(self):
        maingrid = QGridLayout()
        maingrid.addWidget(self.createInfoGroup(), 1, 0)
        maingrid.addWidget(self.createInputGroup(), 0, 0)
        maingrid.addWidget(self.createPlotGroup(), 0, 1)
        maingrid.addWidget(self.createDataGroup(), 1, 1)
        self.setLayout(maingrid)
        self.resize(1300, 800)

        
    def setUpPlwg(self):
        self.plwg = pg.PlotWidget()
        self.plwg.setLabel('left', 'A', color='white', size=20)
        self.plwg.setLabel('bottom', 'Zeit[s]', color='white', size=20)
        self.plwg.showGrid(x=True, y=True)

    def createPlotGroup(self):
        plotGroup = QTabWidget()
        plotGroup.iTab1 = self.iTab1()
        plotGroup.iTab2 = self.iTab2()

        #Add tabs
        plotGroup.addTab(plotGroup.iTab1,"Live Daten")
        plotGroup.addTab(plotGroup.iTab2,"gespeicherte Daten")
        return plotGroup
        
    def iTab1(self):
        plotGroup = QGroupBox()
        vbox = QHBoxLayout()
        self.plwg1 = pg.PlotWidget()
        self.plwg1.setLabel('left', 'A', color='white', size=20)
        self.plwg1.setLabel('bottom', 'Zeit[s]', color='white', size=20)
        self.plwg1.showGrid(x=True, y=True)
        # self.plwg.setYRange(0, 30, padding = 0)
        vbox.addWidget(self.plwg1)
        plotGroup.setLayout(vbox)
        self.gv = pg.GraphicsView()
        self.gv.show()
        return plotGroup
    
    def iTab2(self):
        plotGroup = QGroupBox()
        vbox = QHBoxLayout()
        self.plwg2 = pg.PlotWidget()
        self.plwg2.setLabel('left', 'A', color='white', size=20)
        self.plwg2.setLabel('bottom', 'Zeit[s]', color='white', size=20)
        self.plwg2.showGrid(x=True, y=True)
        # self.plwg.setYRange(0, 30, padding = 0)
        vbox.addWidget(self.plwg2)
        plotGroup.setLayout(vbox)
        self.gv = pg.GraphicsView()
        self.gv.show()
        return plotGroup

        


    def plotter(self):
        self.curveCp1 = self.plwg1.getPlotItem().plot(self.dataTime, self.dataCp, symbol = 's', pen=None,symbolSize=3, symbolBrush=('b'))
        self.curveCpSaved1 = self.plwg1.getPlotItem().plot(self.dataTimeSaved, self.dataCpSaved, symbol = '+', pen=None, symbolSize=30, symbolBrush=('b'))
        self.plot1timer = QtCore.QTimer()
        
        self.curveCp2 = self.plwg2.getPlotItem().plot(self.dataTime, self.dataCp, symbol = 's', pen=None,symbolSize=3, symbolBrush=('b'))
        self.curveCpSaved2 = self.plwg2.getPlotItem().plot(self.dataTimeSaved, self.dataCpSaved, symbol = '+', pen=None, symbolSize=30, symbolBrush=('b'))
        
        
        self.plot1timer.timeout.connect(self.updater)
        # Zeitkompression hier ändern:
        self.plot1timer.start(1000 * self.plotIntervall)
        


    def updater(self):
        if len(self.dataTime1)>1:
            if self.dataTime1[-1]-self.dataTime1[0] > 15:
                self.dataCp1.pop(0)
                self.dataTime1.pop(0)
            try:    
                if self.dataTime1[-1]-self.dataTimeSaved1[0] > 15:
                    self.dataCpSaved1.pop(0)
                    self.dataTimeSaved1.pop(0)
            except:
                    a=1+1 #'Platzhalter'
        self.dataCp1.append(self.data[0][self.dataIndex*10*self.plotIntervall])
        self.dataTime1.append(self.data[1][self.dataIndex*10*self.plotIntervall])    
        self.curveCp1.setData(self.dataTime1, self.dataCp1)
        self.curveCpSaved1.setData(self.dataTimeSaved1, self.dataCpSaved1)
        
        
        self.dataCp2.append(self.data[0][self.dataIndex*10*self.plotIntervall])
        self.dataTime2.append(self.data[1][self.dataIndex*10*self.plotIntervall])    
        self.curveCp2.setData(self.dataTime2, self.dataCp2)
        self.curveCpSaved2.setData(self.dataTimeSaved2, self.dataCpSaved2)
        
        
        
        self.dataIndex = self.dataIndex + 1

            
    
    def startM(self):
        self.plwg1.getPlotItem().clear()
        time.sleep(1)
        self.dataIndex = 0
        self.dataCp = []
        self.dataTime = []
        self.dataCpSaved = []
        self.dataTimeSaved = []
        
        self.plwg1.setYRange(0, self.ca_0+1.1, padding = 0)
        self.start = 1
        self.plotter()
        
    def stopM(self):
        self.plot1timer.stop()
        
                                                               
    
        
    def createInputGroup(self):
        #inputGroup = QGroupBox("Versuchsauswahl")
        inputGroup = QTabWidget()
        inputGroup.tab1 = self.tab1()
        inputGroup.tab2 = self.tab2()
        inputGroup.tab3 = self.tab3()
        # inputGroup.tab4 = self.tab4()
        
        #Add tabs
        inputGroup.addTab(inputGroup.tab1,"Versuch 1")
        inputGroup.addTab(inputGroup.tab2,"Versuch 2")
        inputGroup.addTab(inputGroup.tab3,"Versuch 3")
        
        return inputGroup
        
    def tab1(self):    
        inputGroup = QGroupBox()
       # vbox = QVBoxLayout()
        
        buttonStart = QPushButton("Messung starten")
        buttonStart.clicked.connect(lambda: self.startM1())
        
        buttonStop = QPushButton("Messung anhalten")
        buttonStop.clicked.connect(lambda: self.stopM())
        
        ca_0selection = QGroupBox("A0")
        vbox = QVBoxLayout()
        slider_FlowA = QSlider(Qt.Vertical)
        slider_FlowA.setValue(0)
        # imlement the maximum as an atribut of class later
        slider_FlowA.setMaximum(30)
        slider_FlowA.setTickInterval(0.1)
        slider_FlowA.setSingleStep(0.1)
        spinBox_FlowA = QSpinBox()
        # maybe grey out the spinnbox (no typing)
        spinBox_FlowA.setValue(0)
        spinBox_FlowA.setMinimum(0)
        spinBox_FlowA.setMaximum(30)
        vbox.addWidget(slider_FlowA)
        vbox.addWidget(spinBox_FlowA)
        ca_0selection.setLayout(vbox)
        slider_FlowA.valueChanged.connect(self.setCaSlider)
        slider_FlowA.valueChanged.connect(spinBox_FlowA.setValue)
        spinBox_FlowA.valueChanged.connect(slider_FlowA.setValue)
        
        text = QLabel()
        infoText = "Informationen Versuch 1:"
        text.setText(infoText)

        inputLayout = QGridLayout()
        inputLayout.addWidget(buttonStart, 0, 1)
        inputLayout.addWidget(buttonStop, 1, 1)
        inputLayout.addWidget(ca_0selection, 0, 0)
        #inputLayout.addWidget(text, 1, 1)
        inputGroup.setLayout(inputLayout)
        return inputGroup
               
    def startM1(self):
        self.kinetic = 1
        self.startM()
        
    def tab2(self):    
        inputGroup = QGroupBox()
        vbox = QVBoxLayout()
        
        buttonStart = QPushButton("Messung starten")
        buttonStart.clicked.connect(lambda: self.startM2())
        
        buttonStop = QPushButton("Messung anhalten")
        buttonStop.clicked.connect(lambda: self.stopM())
        
        text = QLabel()
        infoText = "Informationen Versuch 2:"
        text.setText(infoText)
        
        inputLayout = QGridLayout()
        inputLayout.addWidget(buttonStart, 0, 0)
        inputLayout.addWidget(buttonStop, 1, 1)

        #inputLayout.addWidget(text, 1, 0)
        
        inputGroup.setLayout(inputLayout)
        return inputGroup
        
    def startM2(self):
        self.kinetic = 2
        self.setCa_0(20)
        self.startM()
        
    def tab3(self):    
        inputGroup = QGroupBox()
        vbox = QVBoxLayout()
        
        buttonStart = QPushButton("Messung starten")
        buttonStart.clicked.connect(lambda: self.startM3())
        
        buttonStop = QPushButton("Messung anhalten")
        buttonStop.clicked.connect(lambda: self.stopM())
                              
        buttonStop = QPushButton("Messung anhalten")
        buttonStop.clicked.connect(lambda: self.stopM())
        
        T_Selection = QGroupBox("Temperatur [K]:")
        vbox = QVBoxLayout()
        slider_T = QSlider(Qt.Vertical)
        slider_T.setValue(20)
        slider_T.setMinimum(250)
        slider_T.setMaximum(350)
        slider_T.setTickInterval(1)
        slider_T.setSingleStep(1)
        spinBox_T = QSpinBox()
        # implement temperatur Range as an attribute of the class later
        spinBox_T.setValue(20)
        spinBox_T.setMinimum(20)
        spinBox_T.setMaximum(300)
        vbox.addWidget(slider_T)
        vbox.addWidget(spinBox_T)
        T_Selection.setLayout(vbox)
        slider_T.valueChanged.connect(self.setT)
        slider_T.valueChanged.connect(spinBox_T.setValue)
        spinBox_T.valueChanged.connect(slider_T.setValue)
        
        text = QLabel()
        infoText = "Informationen Versuch 2:"
        text.setText(infoText)
        
        inputLayout = QGridLayout()
        inputLayout.addWidget(buttonStart, 0, 1)
        inputLayout.addWidget(T_Selection, 0, 0)
        inputLayout.addWidget(buttonStop, 1, 1)

        #inputLayout.addWidget(text, 1, 1)
        
        inputGroup.setLayout(inputLayout)
        return inputGroup
        
    def startM3(self):
        self.kinetic = 3
        self.setCa_0(20)
        self.startM()
     

    def createDataGroup(self):
        dataBox = QGroupBox()
        # model for data import
        self.model = QtGui.QStandardItemModel(self)
        self.dataTable = QTableView(self)
        self.datasets = 0
        self.dataTable.setModel(self.model)
        tableItem = QTableWidgetItem()

        buttonSave = QPushButton("letzten Messpunkt speichern")
        buttonSave.clicked.connect(lambda: self.saveData())     
        buttonExport = QPushButton("Daten exportieren")
        buttonExport.clicked.connect(lambda: self.exportData())

        topLayout = QHBoxLayout()
        topLayout.addWidget(buttonSave)
        topLayout.addWidget(buttonExport)
        topLayout.addStretch(1)

        dataLayout = QGridLayout()
        dataLayout.addLayout(topLayout, 0, 0, 1, 2)
        dataLayout.addWidget(self.dataTable, 1, 0)
        dataBox.setLayout(dataLayout)
        return dataBox
        
        
        #Zeit ergänzen!!!!!
        
        
    def createEmptyDataDocument(self):
        header = ['Versuch',  'T [°C]', 'cA_0 ', 'cA ','t [s]','Rauschen', 'Trägheit']
        with open('data.csv', 'w') as csv_file:
            csv_writer = csv.writer(csv_file, delimiter=',')
            csv_writer.writerow(header)

    def saveData(self):
            """
            data = [self.getKinetic(), self.Treal, self.getCa_0(), self.dataCp[-1].round(3), self.dataTime[-1].round(),
                    self.rausch[self.dataIndex], self.detTr[self.dataIndex]]
            """
            data = [self.getKinetic(), self.Treal, self.getCa_0(), self.dataCp1[-1].round(3), self.dataTime1[-1].round()]
            with open('data.csv', 'a') as csv_file:
                csv_writer = csv.writer(csv_file, delimiter=',')
                csv_writer.writerow(data)
            self.showData()
            self.dataCpSaved.append(self.dataCp1[-1])
            self.dataTimeSaved.append(self.dataTime1[-1])


    def exportData(self):
        options = QFileDialog.Options()
        # options |= QFileDialog.DontUseNativeDialog
        fileName, _ = QFileDialog.getSaveFileName(self,"QFileDialog.getSaveFileName()","","All Files (*);;CSV Files (*.CSV)", options=options)
        if fileName:
            print("Data exportet to " + fileName + ".csv")
            destination = os.getcwd() + '/data.csv'
            copyfile(destination, fileName + ".csv")
            """
            Achtung! Gleichnamige Dateien werden ungefragt überschrieben!
            """

    def showData(self):
        self.model.clear()
        with open('data.csv', "r") as fileInput:
            for row in csv.reader(fileInput):
                items = [
                        QtGui.QStandardItem(field)
                        for field in row
                        ]
                self.model.appendRow(items)

    def createInfoGroup(self):
        infoBox = QGroupBox("Information")
        vbox = QHBoxLayout()
        text = QLabel()
        infoText = "Hier werden die PNGs eingefügt."
        text.setText(infoText)
        vbox.addWidget(text)
        infoBox.setLayout(vbox)
        return infoBox




# run GUI
if __name__ == '__main__':

    app = QApplication(sys.argv)
    GUI = inSilico_GUI()
    sys.exit(app.exec_())
