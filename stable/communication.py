from random import randrange
from pyqtgraph.Qt import QtCore



class updater():
    def __init__(self, gui, kineticSolver):
        self.data = []
        
        self.timer = QtCore.QTimer()
        self.timer.timeout.connect(lambda: self.update(gui,
                                   kineticSolver))
        self.timer.start(0)


    def update(self, gui, kineticSolver):
        if gui.start == 1:
            self.dataToKineticSolver(gui, kineticSolver)
            kineticSolver.calculate()
            
            gui.data = kineticSolver.data
            gui.rausch = kineticSolver.rausch
            gui.detTr = kineticSolver.detTr

            gui.start = 0

                
    def dataToKineticSolver(self, gui, kineticSolver):
        kineticSolver.setT(gui.getT())
        kineticSolver.setKinetic(gui.getKinetic())
        kineticSolver.setCa_0(gui.getCa_0())

        
