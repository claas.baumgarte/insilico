from PyQt5.QtWidgets import QApplication
import sys

import model
import PyQT_GUI as gui
import PyQT_GUI_Errormessage as error
import communication as com



if __name__ == '__main__':

    app = QApplication(sys.argv)
    GUI = gui.inSilico_GUI()
    kineticSolver = model.kineticSolver()
    updater = com.updater(GUI, kineticSolver)


sys.exit(app.exec_())
