import numpy as np
from scipy.integrate import odeint
from pyqtgraph.Qt import QtCore
import random

class kineticSolver():

    def __init__(self):
        self.T = 250 #[K]
        self.kinetic = 0
        self.Ca_0 = 0.1 
        self.Cb_0 = 0.1
        self.concentration = []
        self.timeArrayMin = np.linspace(0, 20, 12000)
        self.timeArrayS = np.linspace(0, 1200, 12000)
        self.data = [[],[]]
        self.rausch = []
        self.detTr = []

    def setT(self, Tgui):
        self.T = Tgui

    def getTreal(self):
        return self.Treal

    def setKinetic(self, kinetic):
        self.kinetic = kinetic
        
    def getKinetic(self):
        return self.kinetic

    def setCa_0(self, new):
        self.Ca_0 = new
        
    def getConcentration(self):
        return self.concentration
 

    def calculate(self):
        if self.kinetic == 1:
            print("Versuch 1")
            k = 1.00385
            A0 = self.Ca_0 
            t = self.timeArrayMin
            ErstOrdVec = np.vectorize(self.ErsteOrdnung)
            self.concentration = ErstOrdVec(k, t, A0)
        if self.kinetic == 2:
            print("Versuch 2")
            k = 0.1989
            A0 = self.Ca_0
            t = self.timeArrayMin
            ErstOrdVec = np.vectorize(self.ErsteOrdnung)
            self.concentration = ErstOrdVec(k, t, A0)
        if self.kinetic == 3:
            k = 2
            A0 = self.Ca_0
            B0 = self.Cb_0
            t = self.timeArrayMin
            ErstOrdVec = np.vectorize(self.ErsteOrdnung)
            self.concentration = ErstOrdVec(k, t, A0)
        self.data = [self.concentration,self.timeArrayS]
        
        DetTrVec = np.vectorize(self.DetTraeg)
        A = self.concentration
        t = 12 #Zeit?
        K = 123 #Wert einsetzen
        self.detTr = DetTrVec(A,t,K)
        
        RauschenVec = np.vectorize(self.Rauschen)
        A = self.concentration
        t = 12 #Zeit?
        MaxAbweichung = 0.05 #?
        self.ruasch = RauschenVec(t,MaxAbweichung,A)
                 
# kinetics
    ## 1. Ordnung)
    def ErsteOrdnung(self, k,t,A0):
        cA =  A0 * (np.exp(-k*t))
        return cA
    ErstOrdVec = np.vectorize(ErsteOrdnung)

    
    ## 2. Ordnung)
    #Fall 1: A+A->P
    def ZweiteOrdnungF1(self,k,t,A0):
        cA = (A0)/(1+k*t*A0)
        return cA
    ZweitOrdF1Vec = np.vectorize(ZweiteOrdnungF1)

    #Fall 2: A+B->P # A0 =! B0
    def ZweiteOrdnungF2(self, k,t,A0,B0):
        Diff1=A0-B0
        Diff2=B0-A0
        cA = (A0*Diff1*np.exp(Diff1*k*t))/(A0*np.exp(Diff1*k*t)-B0)
        cB = (B0*Diff2*np.exp(Diff2*k*t))/(B0*np.exp(Diff2*k*t)-A0)
        return cA, cB
    ZweitOrdF2Vec = np.vectorize(ZweiteOrdnungF2)

    
    ## Definieren der Funktionen für Messrauschen
    # Detektor Trägheit                                                                                                      
    def DetTraeg(self, A,t,K):
        T = A * np.exp(-K*t)
        return T
    DetTrVec = np.vectorize(DetTraeg)

    # Systematisches Rauschen
    def Rauschen(self, t,MaxAbweichung,Messwert):
        VZ = 1
        if np.random.randint(2) == 1:
            VZ = -1
        Rauschen = Messwert*MaxAbweichung*np.random.random()*VZ
        Traegheit = Messwert * self.DetTraeg(0.03,t,0.3)
        return Rauschen + Messwert + Traegheit
    RauschenVec = np.vectorize(Rauschen)
    
    
    """
        code zum implementieren aller kinetiken:
            k = 2 #platzhalter
            A0 = self.Ca_0 
            t = self.timearray
            self.concentration = ErstOrdVec(k, t, A0)

            k = 2 #platzhalter
            A0 = self.Ca_0
            t = self.timearray
            self.concentration = ZweitOrdF1Vec(k, t, A0)
            
            k = 2 #platzhalter
            A0 = self.Ca_0
            B0 = self.Cb_0
            t = self.timearray
            self.concentration = ZweitOrdF2Vec (k, t, A0, B0)
    """
    
    