import numpy as np
from scipy.integrate import odeint
from pyqtgraph.Qt import QtCore
import random

class kineticSolver():

    def __init__(self):
        self.Tgui = 20 #[°C]
        self.Treal = 20 #[°C]
        self.kinetic = 0
        self.Ca_0 = 0.1 #[mol/l]
        self.Cb_0 = 0.1 #[mol/l]
        self.flow_aGui = 0 #[ml/min]
        self.flow_bGui = 0 #[ml/min]
        self.flow_aReal = 0 #[ml/min]
        self.flow_bReal = 0 #[ml/min]
        self.cp = 0 #[mol/l]
        self.ca = 0 #[mol/l]
        self.cb = 0 #[mol/l]
        self.dwell = 0 #[min]
        self.reactorvolume = 100 #[ml]
        self.calculateLoop()
        
    def setTgui(self, Tgui):
        self.Tgui = Tgui

    def getTreal(self):
        return self.Treal

    def setKinetic(self, kinetic):
        self.kinetic = kinetic

    def setCa_0(self, new):
        self.Ca_0 = new

    def setCb_0(self, new):
        self.Cb_0 = new

    def getCp(self):
        return self.cp

    def getCa(self):
        return self.ca

    def getCb(self):
        return self.cb
    
    
    # kinetics
    def scnd(self, z, t):
        # A + B -> P
        a = z[0]
        b = z[1]
        p = z[2]
        T = z[3]
        k = 40*np.exp(-20/T)
        dadt = -k*a*b
        dbdt = -k*a*b
        dpdt = k*a*b
        dTdt = 0
        return [dadt, dbdt, dpdt, dTdt]

    def noreact(self, z, t):
        return [0, 0, 0, 0]
    
    def pow(self, z, t):
        # 2A + B -> P
        a=z[0]
        b=z[1]
        p=z[2]
        T = z[3]
        k=40*np.exp(-23/T)
        dadt = -2*k*a*a*b
        dbdt =-k*a*a*b
        dpdt = k*a*a*b
        dTdt = 0
        return [dadt,dbdt,dpdt, dTdt]
    
    def calcTreal(self, Tgui):
        Treal = Tgui
        self.Treal = Treal
    
    def calculate(self):
        self.calcTreal(self.Tgui)
        self.calcFlowAreal(self.flow_aGui)
        self.calcFlowbreal(self.flow_bGui)
        self.dwell = self.calc_dwell(self.flow_aReal, self.flow_bReal,
                                     self.reactorvolume)
        
       
    def calc_Concentration(self, kinetic, iniConditions, dwell):
        t = np.linspace(0, self.dwell, 100000)
        if kinetic == 0:
            y = odeint(self.scnd, iniConditions, t)
        if kinetic == 1:
            y = odeint(self.noreact, iniConditions, t)
        if kinetic == 2:
            y = odeint(self.pow, iniConditions, t)
        return y
